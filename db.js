const Sequelize = require("sequelize");


const sequelize = new Sequelize(
    "MURA24", `${process.env.MYSQL_USER}`, `${process.env.MYSQL_PASSWORD}`, 
{
    host: `${process.env.MYSQL_DB_HOST}`, 
    dialect:"mysql",
    port:"3306",
    logging:false
});

const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//import modela
db.nekretnina = require(__dirname+'/nekretnina.js')(sequelize);
db.korisnik = require(__dirname+'/korisnik.js')(sequelize);
db.upit = require(__dirname+'/upit.js')(sequelize);

db.nekretnina.hasMany(db.upit,{as:'NekretninaId'});
db.korisnik.hasMany(db.upit,{as:'KorisnikId'})

module.exports=db;